const Booking = require('../models/Booking');

module.exports = {
    async store(req, res) {
        const { user_id } = req.headers;
        const { sport_id } = req.params;
        const { date } = req.body; 

        const booking = await Booking.create({ 
            user: user_id,
            spot: sport_id,
            date, 
        });

        booking.populate('user').populate('spot').execPopulate();
        
        return res.json(booking);
    }
}